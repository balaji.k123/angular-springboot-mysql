package com.Crud.project.ServerSide.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.Crud.project.ServerSide.Entity.User;

@Repository
public interface User_Repository extends JpaRepository<User, Long>{

}
