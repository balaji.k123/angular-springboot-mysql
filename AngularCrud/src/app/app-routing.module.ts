import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { UpdateEmployeeComponent } from './update-employee/update-employee.component';

const routes: Routes = [
  // { path: '', component: EmployeeListComponent },
  { path: '', redirectTo: 'employeeCreate', pathMatch: 'full' },
  { path: 'employeeList', component: EmployeeListComponent },
  { path: 'employeeCreate', component: CreateEmployeeComponent },
  { path: 'employeeUpdate/:id', component: UpdateEmployeeComponent },
  // { path: 'home', component: CreateEmployeeComponent },
  // { path: 'signUp', component: LoginPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
