import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../service/Employee';
import { EmployeeService } from '../service/employee.service';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],
})
export class EmployeeListComponent implements OnInit {
  employee: Employee[] = [];

  constructor(
    private employeeService: EmployeeService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.getemployeeslist();
    // this.employee = [

    //   { id: 1, fristName: 'balaji', age: 22, phoneNumber: 9930 },
    // ];
  }
  private getemployeeslist() {
    this.employeeService.getEmployees().subscribe((data) => {
      this.employee = data;
    });
  }
  updateById(id: number) {
    this.router.navigate(['employeeUpdate', id]);
  }
  deleteById(id: number) {
    this.employeeService.deleteById(id).subscribe(() => {
      this.getemployeeslist();
    });
  }
}
