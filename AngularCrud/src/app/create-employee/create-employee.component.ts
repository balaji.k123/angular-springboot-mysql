import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../service/Employee';
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css'],
})
export class CreateEmployeeComponent implements OnInit {
  employee: Employee = new Employee();
  constructor(
    public employeeService: EmployeeService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  getEmployeese() {
    this.router.navigate(['/employeeList']);
  }

  onCreate() {
    debugger;
    this.employeeService.createEmployee(this.employee).subscribe(
      (data) => {
        console.log(data);
      },
      (error) => console.log(error)
    );
    this.getEmployeese();
  }
}
