import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../service/Employee';
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css'],
})
export class UpdateEmployeeComponent implements OnInit {
  id!: number;
  employee: Employee = new Employee();
  constructor(
    private employeeUpdate: EmployeeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.employeeUpdate.getEmployeeById(this.id).subscribe(
      (data) => {
        this.employee = data;
      },
      (error) => console.log(error)
    );
  }
  onSubmit() {
    this.employeeUpdate.updateEmployee(this.id, this.employee).subscribe(
      (data) => {
        console.log(data);
        this.getEmployees_3();
      },
      (error) => console.log(error)
    );
  }
  getEmployees_3() {
    this.router.navigate(['/employeeList']);
  }
}
