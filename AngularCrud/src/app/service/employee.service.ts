import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from './Employee';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private ApiUrl = 'http://localhost:8080/api/v1/employee';

  constructor(private http: HttpClient) {}
  //get panna lines
  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${this.ApiUrl}`);
  }

  createEmployee(employee: Employee): Observable<object> {
    return this.http.post(`${this.ApiUrl}`, employee);
  }
  //get panna
  getEmployeeById(id: number): Observable<Employee> {
    return this.http.get<Employee>(`${this.ApiUrl}/${id}`);
  }

  updateEmployee(id: number, employee: Employee): Observable<Object> {
    return this.http.put(`${this.ApiUrl}/${id}`, employee);
  }

  deleteById(id: number): Observable<Object> {
    return this.http.delete(`${this.ApiUrl}/${id}`);
  }
}
